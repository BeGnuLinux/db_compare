<?php

namespace Ecms\DbCompare\Http\Controllers;

use App\Http\Controllers\Controller;
use Doctrine\DBAL\DriverManager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MigrationsController extends Controller
{
  public function saveMigration(Request $request) {
    $this->template($request->all());
    return response()->json([
      'message' => 'Migration file created',
    ]);
  }
  public function template($table) {
      $schema = "<?php

// Migration Created: " . date("Y-m-d H:i:s") . "
// Created by ecms-migrations tool
// --------------------------------------------------
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class " . ucwords($table['name']) . "Table extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */

  public function up()
  {
    Schema::create('". $table['name'] .'\', function (Blueprint $table) {
    ';

        foreach($table['columns'] as $column) {

          $method = '';
          switch ($column['type']) {
            case 'int' :
              $method = 'integer';
              break;
            case 'integer' :
              $method = 'unsignedInteger';
              break;
            case 'biginteger' :
              $method = 'unsignedBigInteger';
              break;
            case 'char' :
            case 'varchar' :
            case 'string' :
              $method = 'string';
              break;
            case 'longtext' :
              $method = 'longText';
              break;
            case 'float' :
              $method = 'float';
              break;
            case 'decimal' :
              $method = 'decimal';
              break;
            case 'tinyint' :
            case 'boolean' :
              $method = 'boolean';
              break;
            case 'date':
              $method = 'date';
              break;
            case 'timestamp' :
              $method = 'timestamp';
              break;
            case 'datetime' :
              $method = 'dateTime';
              break;
            case 'mediumtext' :
              $method = 'mediumText';
              break;
            case 'text' :
              $method = 'text';
              break;
          }


          if($column['default'] != null) {
            $default  = "->default('" . $column['default'] ."')";
          }else {
            $default = '';
          }

          if($column['name'] == 'id') {
            $schema .= '
           $table->'. 'bigIncrements' . "('" . $column['name'] ."')" . $default . ";";
          } else {
            $schema .= '
           $table->'. $method . "('" . $column['name'] ."')" . $default . ";";
          }
        }
    $schema .="
    });
  }

 /**
  * Reverse the migrations.
  *
  * @return void
  */
  public function down()
  {
    Schema::dropIfExists('". $table['name'] . "');
  }
}
";

      $this->write($schema, $table['name']);

      return response()->json(['message' => 'file created'], 200);
    }

  public function write($schema, $table)
  {
    $filename = 'ecms_'. date('Y_m_d_His') . "_create_" . $table . "_table.php";
    $dir = base_path('database/migrations/');
    file_put_contents($dir . "/" . $filename, $schema);
  }
}
