<?php

namespace Ecms\DbCompare\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;

class updateDatabaseTableController extends Controller
{
  private $fromDatabase, $toDatabase, $table;
  public  function __construct(Request $request) {
    $this->fromDatabase = $request->fromDatabase;
    $this->toDatabase = $request->toDatabase;
    $this->table = $request->table;
  }
   public function update(Request $request) {
    $this->checkTableExist($this->toDatabase, $this->table['name']);
    $this->createTable($this->toDatabase, $this->table);
   }

   private function checkTableExist($database, $table) {
     config(['database.connections.mysql' => [
       'driver' => 'mysql',
       'url' => env('DATABASE_URL'),
       'host' => env('DB_HOST', '127.0.0.1'),
       'port' => env('DB_PORT', '3306'),
       'database' => $database,
       'username' => env('DB_USERNAME', 'forge'),
       'password' => env('DB_PASSWORD', ''),
       'unix_socket' => env('DB_SOCKET', ''),
       'charset' => 'utf8mb4',
       'collation' => 'utf8mb4_unicode_ci',
       'prefix' => '',
       'prefix_indexes' => true,
       'strict' => true,
       'engine' => null,
     ]]);
     Schema::dropIfExists($table);
   }

   private function createTable($database, $table) {
     config(['database.connections.mysql' => [
       'driver' => 'mysql',
       'url' => env('DATABASE_URL'),
       'host' => env('DB_HOST', '127.0.0.1'),
       'port' => env('DB_PORT', '3306'),
       'database' => $database,
       'username' => env('DB_USERNAME', 'forge'),
       'password' => env('DB_PASSWORD', ''),
       'unix_socket' => env('DB_SOCKET', ''),
       'charset' => 'utf8mb4',
       'collation' => 'utf8mb4_unicode_ci',
       'prefix' => '',
       'prefix_indexes' => true,
       'strict' => true,
       'engine' => null,
     ]]);

     $schema = Schema::create($this->table['name'], function (Blueprint $table) {
       foreach ($this->table['columns'] as $column) {
         // Method switch
         $method = '';
         switch ($column['type']) {
           case 'integer' :
             $method = 'unsignedInteger';
             break;
           case 'string' :
             $method = 'string';
             break;
           case 'longtext' :
             $method = 'longText';
             break;
           case 'float' :
             $method = 'float';
             break;
           case 'decimal' :
             $method = 'decimal';
             break;
           case 'tinyint' :
           case 'boolean' :
             $method = 'boolean';
             break;
           case 'date':
             $method = 'date';
             break;
           case 'timestamp' :
             $method = 'timestamp';
             break;
           case 'datetime' :
             $method = 'dateTime';
             break;
           case 'mediumtext' :
             $method = 'mediumText';
             break;
           case 'text' :
             $method = 'text';
             break;
         }
         if($column['name'] == 'created_at') {
           $table->timestamps();
         } elseif ($column['name'] == 'updated_at') {
           continue;
         } elseif ($column['name'] == 'id') {
           $table->bigIncrements('id');
         } else {
           $table->$method($column['name'])->default($column['default']);
         }
       }
     });
     return response()->json(['message' => "Table updated to the database"], 200);
   }
}
