<?php

namespace Ecms\DbCompare;

use Illuminate\Support\ServiceProvider;

class DbCompareServiceProvider extends ServiceProvider
{
  /**
   * Register services.
   *
   * @return void
   */
  public function register()
  {
    //

  }

  /**
   * Bootstrap services.
   *
   * @return void
   */
  public function boot()
  {
    // Load Routes from route file
    $this->loadRoutesFrom(__DIR__.'/routes/web.php');
    // Load Config file
    $this->mergeConfigFrom(__DIR__ . '/config/ecmscompare.php', 'ecmscompare');
//    // Publish config file name "ecmscompare.php"
    $this->publishes([__DIR__.'/config/ecmscompare.php' => config_path('ecmscompare.php'),]);
//    // Load translations file
//    $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'translate');
//    // Publish translations files
//    $this->publishes([__DIR__.'/resources/lang' => resource_path('lang/'),]);
//    // Load Views files
    $this->loadViewsFrom(__DIR__.'/resources/views', 'ecmscompare');
//    // Publish views files
    $this->publishes([__DIR__.'/resources/views' => resource_path('views/'),]);
//    // Publish dependency assets
    $this->publishes([__DIR__.'/public' => public_path('db_compare/images'),], 'public');
  }
}

